// const typescriptIsTransformer =
//   require('typescript-is/lib/transform-inline/transformer').default;
const { readdirSync } = require('fs');

const dir = 'src';
const entry = readdirSync(dir)
  .filter((item) => /\.(t|j)s$/.test(item))
  .filter((item) => !/\.d\.(t|j)s$/.test(item))
  .reduce(
    (acc, fileName) => ({
      ...acc,
      [fileName.replace(/\.(t|j)s$/, '')]: `./${dir}/${fileName}`,
    }),
    {}
  );

module.exports = {
  entry,
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        // options: {
        //   getCustomTransformers: (program) => ({
        //     before: [typescriptIsTransformer(program)],
        //   }),
        // },
      },
    ],
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['.tsx', '.ts', '.js', '.json'],
  },
  target: 'node',
  stats: 'minimal',
  optimization: {
    usedExports: true,
  },
};
