import Language from '../enums/language';
import IDrink from '../interfaces/IDrink';
import IGetRandomCocktailResponse from '../interfaces/IGetRandomCocktailResponse';
import IIngredient from '../interfaces/IIngredient';
import * as Constants from '../util/constants';
import * as Randomizer from '../util/randomizer';

const PrefixIngredientName = 'strIngredient';
const PrefixMeasureName = 'strMeasure';
const PrefixInstructions = 'strInstructions';
type ObjectKey = keyof IDrink;

export default function mapDrinkToCocktailResponse(
  drink: IDrink,
  language: Language
): IGetRandomCocktailResponse {
  return {
    language: language,
    title: drink.strDrink,
    instructions: getTranslation(drink, language),
    image: drink.strDrinkThumb,
    ingredients: extractIngredients(drink),
  } as IGetRandomCocktailResponse;
}

function extractIngredients(drink: IDrink): IIngredient[] {
  const ingredients: IIngredient[] = [];

  //TODO: iterate only required properties
  for (const prop in drink) {
    if (prop.startsWith(PrefixIngredientName)) {
      const ingredientIndex = prop.substring(PrefixIngredientName.length);
      const measureProp = `${PrefixMeasureName}${ingredientIndex}` as ObjectKey;
      const ingredientPropValue = drink[prop as ObjectKey];
      const measurePropValue = drink[measureProp];

      if (ingredientPropValue || measurePropValue) {
        ingredients.push({
          name: ingredientPropValue,
          measure: measurePropValue,
          image: ingredientPropValue
            ? `${Constants.ApiImageUrlPrefix}${ingredientPropValue}${Constants.ApiImageExtension}`
            : '',
        } as IIngredient);
      }
    }
  }

  return ingredients;
}

function getTranslation(drink: IDrink, language: Language): string {
  if (language === Language.En) {
    return drink.strInstructions;
  }

  const langProp =
    `${PrefixInstructions}${language.toUpperCase()}` as ObjectKey;

  let translatedStr = drink[langProp];

  if (!translatedStr) {
    translatedStr = Randomizer.getRandomString(drink[PrefixInstructions]);
  }

  return translatedStr;
}
