import { APIGatewayProxyEvent } from 'aws-lambda';
import { getRandomCocktail } from '../handlers/getRandomCocktailHandler';
import Language from '../enums/language';
import axios from 'axios';
import Response from '../util/response';
import IGetRandomCocktailResponse from '../interfaces/IGetRandomCocktailResponse';
import apiResponse from '../tests/data/testApiResponses';
import IIngredient from '../interfaces/IIngredient';
import * as Constants from '../util/constants';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const translated = {
  language: Language.En,
  title: 'test drink name 1',
  instructions: 'how to prepare EN',
  image: 'test image url',
  ingredients: [
    {
      name: 'ingr 1',
      measure: 'measure 1',
      image: `${Constants.ApiImageUrlPrefix}ingr 1${Constants.ApiImageExtension}`,
    } as IIngredient,

    {
      name: 'ingr 2',
      measure: 'measure 2',
      image: `${Constants.ApiImageUrlPrefix}ingr 2${Constants.ApiImageExtension}`,
    } as IIngredient,
  ],
} as IGetRandomCocktailResponse;

const cases = [
  [
    //happy path
    {
      queryStringParameters: {
        language: Language.En,
      },
    } as unknown as APIGatewayProxyEvent,

    Response[200](translated),
  ],

  [
    //lang + random prop
    {
      queryStringParameters: {
        language: Language.En,
        invalidProp: 'invalidVal',
      },
    } as unknown as APIGatewayProxyEvent,

    Response[400]('Invalid parameters'),
  ],

  //only random prop
  [
    {
      queryStringParameters: {
        invalidProp: 'invalidVal',
      },
    } as unknown as APIGatewayProxyEvent,

    Response[400]('Invalid parameters'),
  ],

  //no props
  [
    {
      queryStringParameters: {},
    } as unknown as APIGatewayProxyEvent,

    Response[200](translated),
  ],
];

describe('getRandomCocktailHandler', () => {
  describe('handler returns expected result', () => {
    test.each(cases)(
      'should return 200 or 400',
      async (event, expectedResult) => {
        mockedAxios.get.mockResolvedValue({
          data: apiResponse.validResponse1,
          status: 200,
        });

        const response = await getRandomCocktail(event as APIGatewayProxyEvent);

        expect(response).toEqual(expectedResult);
      }
    );
  });

  describe('api fails', () => {
    it('should return 500', async () => {
      mockedAxios.get.mockResolvedValueOnce({
        data: apiResponse.validResponse1,
        status: 500,
      });

      const response = await getRandomCocktail({
        queryStringParameters: {
          language: Language.En,
        },
      } as unknown as APIGatewayProxyEvent);

      expect(response).toEqual(Response[500]('Error calling api'));
    });
  });

  describe('when language invalid', () => {
    it('should return random language', async () => {
      mockedAxios.get.mockResolvedValueOnce({
        data: apiResponse.validResponse1,
        status: 200,
      });

      const { statusCode, body } = await getRandomCocktail({
        queryStringParameters: {
          language: 'invalid lang',
        },
      } as unknown as APIGatewayProxyEvent);

      const language =
        JSON.parse(body)['message'][Constants.QueryStringParameterLanguage];

      expect(statusCode).toEqual(200);
      expect(language).toEqual(Language.Random);
    });
  });
});
