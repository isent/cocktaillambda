import { APIGatewayProxyEvent } from 'aws-lambda';
import Language from '../enums/language';
import axios from 'axios';
import apiResponse from '../tests/data/testApiResponses';
import * as Constants from '../util/constants';
import extractLanguage from '../util/queryParameterExtractors';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const cases = [
  [
    //no lang
    {
      queryStringParameters: {},
    } as unknown as APIGatewayProxyEvent,

    Constants.DefaultLanguage,
  ],

  [
    //lang empty
    {
      queryStringParameters: {
        language: '',
      },
    } as unknown as APIGatewayProxyEvent,

    Constants.DefaultLanguage,
  ],

  [
    //has valid lang
    {
      queryStringParameters: {
        language: Language.Fr,
      },
    } as unknown as APIGatewayProxyEvent,

    Language.Fr,
  ],

  [
    //has invalid lang
    {
      queryStringParameters: {
        language: 'xx',
      },
    } as unknown as APIGatewayProxyEvent,

    Language.Random,
  ],
];

describe('extractLanguage', () => {
  test.each(cases)(
    'should return expected language',
    async (event, expectedResult) => {
      mockedAxios.get.mockResolvedValue({
        data: apiResponse.validResponse1,
        status: 200,
      });

      const language = extractLanguage(event as APIGatewayProxyEvent);

      expect(language).toEqual(expectedResult);
    }
  );
});
