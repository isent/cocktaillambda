import Language from '../enums/language';
import apiResponse from '../tests/data/testApiResponses';
import * as Constants from '../util/constants';
import mapDrinkToCocktailResponse from '../mappers/drinkToGetRandomCocktailResponseMapper';
import IDrink from '../interfaces/IDrink';
import * as Randomizer from '../util/randomizer';

const cases = [
  [
    //map when lang and ingredients correct
    apiResponse.validResponse1.drinks[0],
    Language.Fr,
    {
      language: Language.Fr,
      title: apiResponse.validResponse1.drinks[0].strDrink,
      instructions: apiResponse.validResponse1.drinks[0].strInstructionsFR,
      image: apiResponse.validResponse1.drinks[0].strDrinkThumb,
      ingredients: [
        {
          name: apiResponse.validResponse1.drinks[0].strIngredient1,
          measure: apiResponse.validResponse1.drinks[0].strMeasure1,
          image: `${Constants.ApiImageUrlPrefix}${apiResponse.validResponse1.drinks[0].strIngredient1}${Constants.ApiImageExtension}`,
        },

        {
          name: apiResponse.validResponse1.drinks[0].strIngredient2,
          measure: apiResponse.validResponse1.drinks[0].strMeasure2,
          image: `${Constants.ApiImageUrlPrefix}${apiResponse.validResponse1.drinks[0].strIngredient2}${Constants.ApiImageExtension}`,
        },
      ],
    },
  ],

  [
    //map when lang is specialcased and ingredients correct
    apiResponse.validResponse1.drinks[0],
    Language.En,
    {
      language: Language.En,
      title: apiResponse.validResponse1.drinks[0].strDrink,
      instructions: apiResponse.validResponse1.drinks[0].strInstructions,
      image: apiResponse.validResponse1.drinks[0].strDrinkThumb,
      ingredients: [
        {
          name: apiResponse.validResponse1.drinks[0].strIngredient1,
          measure: apiResponse.validResponse1.drinks[0].strMeasure1,
          image: `${Constants.ApiImageUrlPrefix}${apiResponse.validResponse1.drinks[0].strIngredient1}${Constants.ApiImageExtension}`,
        },

        {
          name: apiResponse.validResponse1.drinks[0].strIngredient2,
          measure: apiResponse.validResponse1.drinks[0].strMeasure2,
          image: `${Constants.ApiImageUrlPrefix}${apiResponse.validResponse1.drinks[0].strIngredient2}${Constants.ApiImageExtension}`,
        },
      ],
    },
  ],

  [
    //map when lang correct and no ingredients
    apiResponse.noIngredients.drinks[0],
    Language.En,
    {
      language: Language.En,
      title: apiResponse.noIngredients.drinks[0].strDrink,
      instructions: apiResponse.noIngredients.drinks[0].strInstructions,
      image: apiResponse.noIngredients.drinks[0].strDrinkThumb,
      ingredients: [],
    },
  ],

  [
    //map when lang correct and ingredient has no matching measure
    apiResponse.ingredientWithNoMatchingMeasure.drinks[0],
    Language.En,
    {
      language: Language.En,
      title: apiResponse.ingredientWithNoMatchingMeasure.drinks[0].strDrink,
      instructions:
        apiResponse.ingredientWithNoMatchingMeasure.drinks[0].strInstructions,

      image:
        apiResponse.ingredientWithNoMatchingMeasure.drinks[0].strDrinkThumb,

      ingredients: [
        {
          name: apiResponse.ingredientWithNoMatchingMeasure.drinks[0]
            .strIngredient1,

          measure: null as unknown as string,
          image: `${Constants.ApiImageUrlPrefix}${apiResponse.ingredientWithNoMatchingMeasure.drinks[0].strIngredient1}${Constants.ApiImageExtension}`,
        },
      ],
    },
  ],

  [
    //map when lang correct and measure has no matching ingredient
    apiResponse.measureWithNoMatchingIngredient.drinks[0],
    Language.En,
    {
      language: Language.En,
      title: apiResponse.measureWithNoMatchingIngredient.drinks[0].strDrink,
      instructions:
        apiResponse.measureWithNoMatchingIngredient.drinks[0].strInstructions,

      image:
        apiResponse.measureWithNoMatchingIngredient.drinks[0].strDrinkThumb,

      ingredients: [
        {
          name: null as unknown as string,
          measure:
            apiResponse.measureWithNoMatchingIngredient.drinks[0].strMeasure1,

          image: '',
        },
      ],
    },
  ],
];

describe('drinkToGetRandomCocktailResponseMapper', () => {
  test.each(cases)(
    'should map correctly',
    async (drink, language, expectedResult) => {
      const cocktail = mapDrinkToCocktailResponse(
        drink as unknown as IDrink,
        language as Language
      );

      expect(cocktail).toEqual(expectedResult);
    }
  );

  describe('when language is random', () => {
    it('should call randomizer function and have string of min length 3', async () => {
      const randomStrFunc = jest.spyOn(Randomizer, 'getRandomString');

      const cocktail = mapDrinkToCocktailResponse(
        {
          strInstructions: 'how to prepare EN',
        } as IDrink,
        Language.Random
      );

      expect(randomStrFunc).toHaveBeenCalled();
      expect(cocktail.instructions.length).toBeGreaterThanOrEqual(3);
      randomStrFunc.mockRestore();
    });
  });
});
