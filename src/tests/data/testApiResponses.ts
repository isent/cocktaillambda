import IDrink from '../../interfaces/IDrink';
import IGetDrinkResponse from '../../interfaces/IGetDrinkResponse';

const apiResponse = {
  validResponse1: {
    drinks: [
      {
        strDrink: 'test drink name 1',
        strDrinkThumb: 'test image url',
        strInstructions: 'how to prepare EN',
        strInstructionsFR: 'how to prepare FR',
        strIngredient1: 'ingr 1',
        strIngredient2: 'ingr 2',
        strMeasure1: 'measure 1',
        strMeasure2: 'measure 2',
      } as unknown as IDrink,
    ],
  } as IGetDrinkResponse,

  noIngredients: {
    drinks: [
      {
        strDrink: 'test drink name 1',
        strDrinkThumb: 'test image url',
        strInstructions: 'how to prepare EN',
        strInstructionsFR: 'how to prepare FR',
      } as unknown as IDrink,
    ],
  } as IGetDrinkResponse,

  ingredientWithNoMatchingMeasure: {
    drinks: [
      {
        strDrink: 'test drink name 1',
        strDrinkThumb: 'test image url',
        strInstructions: 'how to prepare EN',
        strInstructionsFR: 'how to prepare FR',
        strIngredient1: 'ingr 1',
        strMeasure1: null,
      } as unknown as IDrink,
    ],
  } as IGetDrinkResponse,

  measureWithNoMatchingIngredient: {
    drinks: [
      {
        strDrink: 'test drink name 1',
        strDrinkThumb: 'test image url',
        strInstructions: 'how to prepare EN',
        strInstructionsFR: 'how to prepare FR',
        strIngredient1: null,
        strMeasure1: 'measure 1',
      } as unknown as IDrink,
    ],
  } as IGetDrinkResponse,
};

export default apiResponse;
