import { APIGatewayProxyEvent } from 'aws-lambda';
import Language from '../enums/language';
import axios from 'axios';
import validateQueryStringParameters from '../util/validators/validateQueryStringParameters';
import apiResponse from '../tests/data/testApiResponses';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const cases = [
  [
    //no props
    {
      queryStringParameters: {},
    } as unknown as APIGatewayProxyEvent,

    true,
  ],

  [
    //has only lang
    {
      queryStringParameters: {
        language: Language.Fr,
      },
    } as unknown as APIGatewayProxyEvent,

    true,
  ],

  [
    //has lang + random prop
    {
      queryStringParameters: {
        language: Language.Fr,
        invalidProp: 'invalidVal',
      },
    } as unknown as APIGatewayProxyEvent,

    false,
  ],

  [
    //has only random prop
    {
      queryStringParameters: {
        invalidProp: 'invalidVal',
      },
    } as unknown as APIGatewayProxyEvent,

    false,
  ],
];

describe('validateQueryStringParameters', () => {
  test.each(cases)(
    'should return expected validation result',
    async (event, expectedResult) => {
      mockedAxios.get.mockResolvedValue({
        data: apiResponse.validResponse1,
        status: 200,
      });

      const isValid = validateQueryStringParameters(
        event as APIGatewayProxyEvent
      );

      expect(isValid).toEqual(expectedResult);
    }
  );
});
