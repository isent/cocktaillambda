import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import * as Constants from '../util/constants';
import Response from '../util/response';
import axios from 'axios';
import IGetDrinkResponse from '../interfaces/IGetDrinkResponse';
import mapDrinkToCocktailResponse from '../mappers/drinkToGetRandomCocktailResponseMapper';
import validateQueryStringParameters from '../util/validators/validateQueryStringParameters';
import extractLanguage from '../util/queryParameterExtractors';

export async function getRandomCocktail(
  event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> {
  if (!validateQueryStringParameters(event)) {
    return Response[400]('Invalid parameters');
  }

  const language = extractLanguage(event);

  const {
    data: {
      drinks: [drink],
    },
    status,
  } = await axios.get<IGetDrinkResponse>(Constants.ApiUrl);

  if (status !== 200) {
    return Response[500]('Error calling api');
  }

  const translatedRecipe = mapDrinkToCocktailResponse(drink, language);

  return Response[200](translatedRecipe);
}
