export default interface IIngredient {
  name: string;
  measure: string;
  image: string;
}
