import IDrink from './IDrink';

export default interface IGetDrinkResponse {
  drinks: IDrink[];
}
