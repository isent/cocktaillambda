import Language from '../enums/language';
import IIngredient from './IIngredient';

export default interface IGetRandomCocktailResponse {
  language: Language;
  title: string;
  instructions: string;
  image: string;
  ingredients: IIngredient[];
}
