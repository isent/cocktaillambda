import Language from '../enums/language';

export const DefaultLanguage = Language.En;
export const ApiUrl = 'https://www.thecocktaildb.com/api/json/v1/1/random.php';
export const ApiImageUrlPrefix =
  'https://www.thecocktaildb.com/images/ingredients/';

export const ApiImageExtension = '.png';
export const QueryStringParameterLanguage = 'language';
