import { APIGatewayProxyEvent } from 'aws-lambda';
import * as Constants from '../constants';

//Ensure parameters meets the requirement: no parameters or only 'language'
export default function validateQueryStringParameters(
  event: APIGatewayProxyEvent
): boolean {
  const params = event.queryStringParameters || {};
  const paramsLength = Object.keys(params).length;

  return (
    paramsLength === 0 ||
    (paramsLength === 1 && Constants.QueryStringParameterLanguage in params)
  );
}
