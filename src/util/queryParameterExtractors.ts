import { APIGatewayProxyEvent } from 'aws-lambda';
import Language from '../enums/language';
import IGetRandomCocktailRequest from '../interfaces/IGetRandomCocktailRequest';
import * as Constants from '../util/constants';

export default function extractLanguage(event: APIGatewayProxyEvent): Language {
  const { language } =
    event.queryStringParameters as unknown as IGetRandomCocktailRequest;

  if (!language) {
    return Constants.DefaultLanguage;
  } else {
    let selectedLanguage = Object.values(Language).find((x) => x === language);

    if (!selectedLanguage) {
      selectedLanguage = Language.Random;
    }

    return selectedLanguage;
  }
}
