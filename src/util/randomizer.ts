//Get random int in a range of [min, max)
export function getRandomInt(min: number, max: number) {
  return Math.floor(Math.random() * (max - min)) + min;
}

export function getRandomString(str: string): string {
  const words = str.split(' ');

  for (let i = 0; i < words.length; i++) {
    const length = getRandomInt(3, 16);
    let randomWord = '';
    let count = 0;

    while (count <= length) {
      //Get random char between 'a' and 'z' inclusively
      const char = String.fromCharCode(getRandomInt(65, 91));
      randomWord += char.toLowerCase();
      count++;
    }

    words[i] = randomWord;
  }

  return words.join(' ');
}
