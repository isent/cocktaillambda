import { APIGatewayProxyResult } from 'aws-lambda';
import IGetRandomCocktailResponse from '../interfaces/IGetRandomCocktailResponse';

//TODO: change to strongly typed return
//TODO: refactor duplication
const Response = {
  200: (msg: IGetRandomCocktailResponse): APIGatewayProxyResult => {
    return {
      statusCode: 200,
      body: JSON.stringify({ message: msg }),
    };
  },

  400: (msg: string): APIGatewayProxyResult => {
    return {
      statusCode: 400,
      body: JSON.stringify({ message: msg }),
    };
  },

  500: (msg: string): APIGatewayProxyResult => {
    return {
      statusCode: 500,
      body: JSON.stringify({ message: msg }),
    };
  },
};

export default Response;
