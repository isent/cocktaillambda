enum Language {
  En = 'en',
  Fr = 'fr',
  Random = 'random',
}

export default Language;
