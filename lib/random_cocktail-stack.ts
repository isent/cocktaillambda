import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apiGateway from 'aws-cdk-lib/aws-apigateway';
import { NodejsFunction } from 'aws-cdk-lib/aws-lambda-nodejs';
import * as path from 'path';

export class RandomCocktailStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const getRandomCocktailLambda = new NodejsFunction(
      this,
      'RandomCocktailHandler',
      {
        runtime: lambda.Runtime.NODEJS_14_X,
        handler: 'getRandomCocktail',
        entry: path.join(
          __dirname,
          `/../src/handlers/getRandomCocktailHandler.ts`
        ),
        bundling: {
          minify: true,
          externalModules: ['aws-sdk'],
        },
      }
    );

    const api = new apiGateway.RestApi(this, 'cocktailApi');
    const resource = api.root.addResource('random-cocktail');
    resource.addMethod(
      'GET',
      new apiGateway.LambdaIntegration(getRandomCocktailLambda)
    );
  }
}
