## Api url

https://6plrvfhb24.execute-api.eu-central-1.amazonaws.com/prod/random-cocktail/?language=en

Available languages: en, fr, random.

## commands

- `npm run build` compile typescript to js
- `npm run test` run tests
- `npm run test-coverage` run tests with coverage results
- `npm run lint` run linting
- `npm run lint:fix` fix linting errors
